package net.lafox.net.imageKeeperClient;

import static net.lafox.net.imageKeeperClient.ImageKeeperClientUtils.upload;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import net.lafox.imageKeeper.ds.ImageUrl;

@SuppressWarnings("WeakerAccess")
public class ImageKeeperClient {
    private final String url;
    private final String imageKeeperUrl;
    private final String fileKeeperUrl;
    private final String userToken;

    public ImageKeeperClient(String url, String userToken) {
        this.url = url;
        this.imageKeeperUrl = url + (url.endsWith("/") ? "uploadImage" : "/uploadImage");
        this.fileKeeperUrl = url + (url.endsWith("/") ? "uploadFile" : "/uploadFile");
        this.userToken = userToken;
    }

    public String getUrl() {
        return url;
    }

    public String getUserToken() {
        return userToken;
    }

    public ImageUrl getImageUrl() {
        return ImageUrl.newInstance(url);
    }

    public String uploadImage(InputStream initialStream) {
        return upload(initialStream, imageKeeperUrl, userToken);
    }

    public String uploadFile(InputStream initialStream) {
        return upload(initialStream, fileKeeperUrl, userToken);
    }

    public String uploadImage(byte[] bytes) {
        return upload(new ByteArrayInputStream(bytes), imageKeeperUrl, userToken);
    }

    public String uploadFile(byte[] bytes) {
        return upload(new ByteArrayInputStream(bytes), fileKeeperUrl, userToken);
    }

    public byte[] downloadFileAsByteArray(String fileName) {
        return ImageKeeperClientUtils.downloadAsByteArray(fileKeeperUrl + "/" + fileName);
    }

    public InputStream downloadFileAsInputStream(String fileName) {
        return ImageKeeperClientUtils.downloadAsInputSream(fileKeeperUrl + "/" + fileName);
    }

    public byte[] downloadImageAsByteArray(String fileName) {
        return ImageKeeperClientUtils.downloadAsByteArray(imageKeeperUrl + "/" + fileName);
    }

    public InputStream downloadImageAsInputStream(String fileName) {
        return ImageKeeperClientUtils.downloadAsInputSream(imageKeeperUrl + "/" + fileName);
    }
}
