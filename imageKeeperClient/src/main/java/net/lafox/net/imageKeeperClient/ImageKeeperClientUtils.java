package net.lafox.net.imageKeeperClient;

import static net.lafox.imageKeeper.utils.apache.StringUtils.isNotEmpty;
import static org.apache.commons.io.FileUtils.copyInputStreamToFile;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import net.lafox.imageKeeper.ds.UploadResponse;
import net.lafox.imageKeeper.exceptions.ImageKeeperException;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.codehaus.jackson.map.ObjectMapper;

class ImageKeeperClientUtils {
    private final static ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    static UploadResponse upload(File file, String imageKeeperUrl, String userToken) {
        try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
            HttpPost httpPost = new HttpPost(imageKeeperUrl);

            HttpEntity httpEntity = MultipartEntityBuilder.create()
                    .addPart("file", new FileBody(file))
                    .addPart("token", new StringBody(userToken, ContentType.TEXT_PLAIN))
                    .build();
            httpPost.setEntity(httpEntity);

            try (CloseableHttpResponse response = httpClient.execute(httpPost)) {
                return getImageIdFromJson(response);
            }
        } catch (Exception e) {
            throw new ImageKeeperException(e);
        }
    }

    static byte[] downloadAsByteArray(String url) {
        try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
            HttpGet httpGet = new HttpGet(url);
            CloseableHttpResponse response = httpClient.execute(httpGet);
            return EntityUtils.toByteArray(response.getEntity());
        } catch (IOException e) {
            throw new ImageKeeperException(e);
        }
    }

    static InputStream downloadAsInputSream(String url) {
        try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
            HttpGet httpGet = new HttpGet(url);
            CloseableHttpResponse response = httpClient.execute(httpGet);
            return response.getEntity().getContent();
        } catch (IOException e) {
            throw new ImageKeeperException(e);
        }
    }


    private static UploadResponse getImageIdFromJson(CloseableHttpResponse response) throws IOException {
        HttpEntity httpEntity = response.getEntity();
        return OBJECT_MAPPER.readValue(httpEntity.getContent(), UploadResponse.class);
    }

    private static File createTempFile(InputStream initialStream) {
        try {
            File tempFile = File.createTempFile("ImageKeeper-uploaded-file-", "");
            copyInputStreamToFile(initialStream, tempFile);
            return tempFile;
        } catch (IOException e) {
            throw new ImageKeeperException(e);
        }
    }

    private static void deleteTempFile(File tempFile) {
        if (!tempFile.delete()) {
            throw new ImageKeeperException("Error deleteTempFile:" + tempFile.getAbsolutePath() + " not deleted.");
        }
    }

    static String upload(InputStream initialStream, String imageKeeperUrl, String userToken) {
        File tempFile = createTempFile(initialStream);
        UploadResponse res = ImageKeeperClientUtils.upload(tempFile, imageKeeperUrl, userToken);
        if (isNotEmpty(res.getErrorMsg())) {
            throw new ImageKeeperException("Client Error: " + res.getErrorMsg());
        }
        deleteTempFile(tempFile);
        return res.getId();
    }
}
