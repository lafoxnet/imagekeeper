package net.lafox.net.imageKeeperClient;


import java.io.FileInputStream;
import java.io.IOException;

import net.lafox.imageKeeper.ds.ImageId;
import net.lafox.imageKeeper.ds.ImageUrl;
import net.lafox.imageKeeper.enums.ImageType;
import net.lafox.imageKeeper.enums.ResizeOperation;

public class Main {
    private static final String HOST = "http://localhost:8888";

    public static void main(String[] args) throws IOException {
        ImageKeeperClient imageKeeperClient = new ImageKeeperClient(HOST, "scienceToken");
        String file ="/Users/tsyma/Downloads/DSC_4999.jpg";

        String imageId = imageKeeperClient.uploadImage(new FileInputStream(file));
        System.out.println("imageId = " + imageId);

        ImageUrl url = ImageUrl.newInstance(HOST)
                .width(352).height(198)
                .imageId(ImageId.getInstance(imageId))
                .imageType(ImageType.JPG)
                .resizeOperation(ResizeOperation.CROP)
                .quality(50);
        System.out.println("url = " + url.generateUrl());
//
        byte[] bytes = imageKeeperClient.downloadImageAsByteArray(url.generateFileName());
        System.out.println("bytes = " + bytes.length);
//
//
//        response = imageKeeperClient.uploadFile(file);
//        byte[] bytesFile = imageKeeperClient.downloadFileAsByteArray(response.getId());
//        byte[] bytesOrig = Files.readAllBytes(file.toPath());
//
//        if (Arrays.equals(bytesOrig, bytesFile)) {
//            System.out.println("Yup, they're the same!");
//        } else {
//            throw new RuntimeException("Arrays is different!!!!");
//        }
    }
}
