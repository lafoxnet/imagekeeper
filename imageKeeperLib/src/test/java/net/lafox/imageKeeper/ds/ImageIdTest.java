package net.lafox.imageKeeper.ds;

import net.lafox.imageKeeper.exceptions.ImageIdException;
import org.junit.Assert;
import org.junit.Test;

public class ImageIdTest {
    @Test(expected = ImageIdException.class)
    public void getInstance_NULL() throws Exception {
        ImageId.getInstance(null);
    }

    @Test(expected = ImageIdException.class)
    public void getInstance_EMPTY() throws Exception {
        ImageId.getInstance("");
    }

    @Test(expected = ImageIdException.class)
    public void getInstance_SHORT() throws Exception {
        ImageId.getInstance("123");
    }

    @Test(expected = ImageIdException.class)
    public void getInstance_LONG() throws Exception {
        ImageId.getInstance("123456789");
    }

    @Test(expected = ImageIdException.class)
    public void getInstance_BAD_FORMAT() throws Exception {
        ImageId.getInstance("123!!!");
    }

    @Test()
    public void getInstance_OK() throws Exception {
        Assert.assertNotNull(ImageId.getInstance("a2B3c412"));
    }
}