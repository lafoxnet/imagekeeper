package net.lafox.imageKeeper.ds;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import net.lafox.imageKeeper.enums.ResizeOperation;
import net.lafox.imageKeeper.exceptions.ImageFormatException;
import org.junit.Test;

public class ImageFormatTest {
    @Test
    public void testConstructor() throws Exception {
        ImageFormat imageFormat = new ImageFormat("c1920x1080q50.jpg");
        assertThat(imageFormat.getExt(), is("jpg"));
        assertThat(imageFormat.getWidth(), is(1920));
        assertThat(imageFormat.getHeight(), is(1080));
        assertThat(imageFormat.getQuality(), is(50));
        assertThat(imageFormat.toString(), is("c1920x1080q50.jpg"));
        assertThat(imageFormat.getResizeOperation(), is(ResizeOperation.CROP));
        assertEquals(imageFormat.getQualityAsDouble(), 0.5, 0.01);
    }

    @Test(expected = ImageFormatException.class)
    public void testBadFormat() throws Exception {
        new ImageFormat("c19201080q50.jpg");
    }

    @Test(expected = ImageFormatException.class)
    public void testNullFormat() throws Exception {
        new ImageFormat(null);
    }
}