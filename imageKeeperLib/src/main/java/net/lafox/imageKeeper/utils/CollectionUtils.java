package net.lafox.imageKeeper.utils;

import java.util.ArrayList;
import java.util.Collection;

@SuppressWarnings("WeakerAccess")
public class CollectionUtils {
    public static <E> ArrayList<E> newArrayList() {
        return new ArrayList<>();
    }

    public static boolean isEmpty(final Collection<?> coll) {
        return coll == null || coll.isEmpty();
    }


    public static boolean isNotEmpty(final Collection<?> coll) {
        return !isEmpty(coll);
    }

}
