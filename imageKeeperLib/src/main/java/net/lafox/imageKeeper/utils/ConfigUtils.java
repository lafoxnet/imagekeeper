package net.lafox.imageKeeper.utils;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import net.lafox.imageKeeper.utils.apache.StringUtils;

public class ConfigUtils {
    private final static Set<String> ENABLED = new HashSet<>(Arrays.asList("enabled", "true", "yes", "1", "y", "t", "e"));

    public static boolean stringToBoolean(String v) {
        return StringUtils.isNotEmpty(v) && ENABLED.contains(v.toLowerCase());

    }

}
