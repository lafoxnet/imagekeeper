package net.lafox.imageKeeper.enums;


public enum ResizeOperation {
    CROP('c'),
    EXPAND('e'),
    WIDTH('w'),
    HEIGHT('h'),
    ORIGINAL('o');

    private final char shortName;

    ResizeOperation(char shortName) {
        this.shortName = shortName;
    }

    public char asChar() {
        return shortName;
    }

    public static ResizeOperation fromChar(String v) {
        if (v == null || v.length() != 1)
            throw new IllegalArgumentException("String '" + v + "' not in character set [cewho]");
        return fromChar(v.charAt(0));
    }

    public static ResizeOperation fromChar(char v) {
        for (ResizeOperation c : ResizeOperation.values()) {
            if (c.shortName == v) {
                return c;
            }
        }
        throw new IllegalArgumentException("Character '" + v + "' is not on set [cewho]");
    }
}
