package net.lafox.imageKeeper.enums;

public enum ImageType {
    PNG("png"),
    JPG("jpg"),
    GIF("gif");
    private final String fileExtension;

    ImageType(String fileExtension) {
        this.fileExtension = fileExtension;
    }

    public static ImageType fromFileExtension(String fileExtension) {
        for (ImageType imageType : ImageType.values()) {
            if (imageType.fileExtension.equalsIgnoreCase(fileExtension)) {
                return imageType;
            }
        }
        throw new IllegalArgumentException("File Extension '" + fileExtension + "' is not available");
    }

    public String getFileExtension() {
        return fileExtension;
    }
}
