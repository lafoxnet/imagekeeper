package net.lafox.imageKeeper.ds;

public class FileDto {
    private final FileId fileId;
    private final Client client;

    public FileDto(FileId fileId, Client client) {
        this.fileId = fileId;
        this.client = client;
    }

    public FileId getFileId() {
        return fileId;
    }

    public Client getClient() {
        return client;
    }

    @Override
    public String toString() {
        return fileId == null ? "NULL" : fileId.getId();
    }
}
