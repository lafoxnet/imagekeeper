package net.lafox.imageKeeper.ds;


import net.lafox.imageKeeper.enums.ImageType;
import net.lafox.imageKeeper.enums.ResizeOperation;

public class ImageUrl {
    private final String imageKeeperUrl;
    private ImageId imageId;
    private int width = 0;
    private int height = 0;
    private ResizeOperation resizeOperation = ResizeOperation.WIDTH;

    private int quality = 60;
    private ImageType imageType = ImageType.JPG;


    public static ImageUrl newInstance(String imageKeeperUrl) {
        return new ImageUrl(imageKeeperUrl);
    }

    public String generateUrl() {
        return imageKeeperUrl + generateFileName();
    }

    public String generateFileName() {
        return (imageId == null ? "NULL" : imageId.toString()) +
                '-' +
                resizeOperation.asChar() +
                width +
                'x' +
                height +
                'q' +
                quality +
                '.' +
                imageType.getFileExtension();
    }

    private ImageUrl(String imageKeeperUrl) {
        if (imageKeeperUrl == null) {
            imageKeeperUrl = "/";
        } else if (!imageKeeperUrl.endsWith("/")) {
            imageKeeperUrl += "/";
        }
        this.imageKeeperUrl = imageKeeperUrl;
    }

    public ImageUrl imageId(ImageId val) {
        imageId = val;
        return this;
    }

    public ImageUrl imageId(String val) {
        imageId = ImageId.getInstance(val);
        return this;
    }

    public ImageUrl width(int val) {
        width = val;
        return this;
    }

    public ImageUrl height(int val) {
        height = val;
        return this;
    }

    public ImageUrl resizeOperation(ResizeOperation val) {
        resizeOperation = val;
        return this;
    }

    public ImageUrl quality(int val) {
        quality = val;
        return this;
    }

    public ImageUrl imageType(ImageType val) {
        imageType = val;
        return this;
    }

}

