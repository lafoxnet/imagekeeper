package net.lafox.imageKeeper.ds;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.lafox.imageKeeper.enums.ResizeOperation;
import net.lafox.imageKeeper.exceptions.ImageFormatException;
import net.lafox.imageKeeper.utils.apache.StringUtils;
import org.springframework.http.InvalidMediaTypeException;
import org.springframework.http.MediaType;

public class ImageFormat {
    @SuppressWarnings("SpellCheckingInspection")
    private final static String PATTERN_STRING = "([cewho])(\\d+)x(\\d+)q(\\d+).(jpg|png|gif)";
    private final static Pattern PATTERN = Pattern.compile(PATTERN_STRING);

    private final int width;
    private final int height;
    private final ResizeOperation resizeOperation;
    private final int quality;
    private final String ext;
    private final MediaType mediaType;
    private final String toString;

    public ImageFormat(String imageFormatString) {
        if (StringUtils.isEmpty(imageFormatString)) throw new ImageFormatException("Empty format string");
        Matcher matcher = PATTERN.matcher(imageFormatString);
        if (matcher.find()) {
            resizeOperation = ResizeOperation.fromChar(matcher.group(1).charAt(0));
            width = Integer.parseInt(matcher.group(2));
            height = Integer.parseInt(matcher.group(3));
            quality = Integer.parseInt(matcher.group(4));
            ext = matcher.group(5);
            mediaType = calcMediaType(ext);
            toString = calcToString();
        } else {
            throw new ImageFormatException("Bad 'Image Format' string. Example:c1920x1080q50.jpg. See comments in application.properties");
        }
    }

    private String calcToString() {
        return "" + resizeOperation.asChar() + width + 'x' + height + 'q' + quality + '.' + ext;
    }

    private MediaType calcMediaType(String ext) {
        MediaType mediaType;
        switch (ext) {
            case "png":
                mediaType = MediaType.IMAGE_PNG;
                break;
            case "jpg":
                mediaType = MediaType.IMAGE_JPEG;
                break;
            case "gif":
                mediaType = MediaType.IMAGE_GIF;
                break;
            default: {
                throw new InvalidMediaTypeException(ext, "Extension " + ext + "Can not be converted to MediaType");
            }
        }
        return mediaType;
    }


    public double getQualityAsDouble() {
        return ((double) quality) / 100.0;
    }


    @Override
    public String toString() {
        return toString;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ImageFormat that = (ImageFormat) o;

        return toString != null ? toString.equals(that.toString) : that.toString == null;
    }

    @Override
    public int hashCode() {
        return toString != null ? toString.hashCode() : 0;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public ResizeOperation getResizeOperation() {
        return resizeOperation;
    }

    public int getQuality() {
        return quality;
    }

    public String getExt() {
        return ext;
    }

    public MediaType getMediaType() {
        return mediaType;
    }
}
