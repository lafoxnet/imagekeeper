package net.lafox.imageKeeper.ds;

import java.io.Serializable;

public class FileId extends AbstractId implements Serializable {

    private FileId(String id) {
        super(id);
    }

    static public FileId getInstance(String id) {
        return new FileId(id);
    }

}
