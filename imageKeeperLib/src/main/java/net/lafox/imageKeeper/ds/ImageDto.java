package net.lafox.imageKeeper.ds;

public class ImageDto {
    private final ImageId imageId;
    private final ImageFormat imageFormat;
    private final Client client;

    private ImageDto(Builder builder) {
        imageId = builder.imageId;
        imageFormat = builder.imageFormat;
        client = builder.client;
    }

    public static Builder Builder() {
        return new Builder();
    }


    @Override
    public String toString() {
        return imageId + "-" + imageFormat;
    }

    public static final class Builder {
        private ImageId imageId;
        private ImageFormat imageFormat;
        private Client client;

        Builder() {
        }

        public Builder imageId(ImageId val) {
            imageId = val;
            return this;
        }

        public Builder imageFormat(ImageFormat val) {
            imageFormat = val;
            return this;
        }

        public Builder client(Client val) {
            client = val;
            return this;
        }

        public ImageDto build() {
            return new ImageDto(this);
        }
    }

    public ImageId getImageId() {
        return imageId;
    }

    public ImageFormat getImageFormat() {
        return imageFormat;
    }

    public Client getClient() {
        return client;
    }
}
