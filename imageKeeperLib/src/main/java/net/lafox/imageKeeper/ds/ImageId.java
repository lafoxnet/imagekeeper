package net.lafox.imageKeeper.ds;

import java.io.Serializable;

public class ImageId extends AbstractId implements Serializable {

    private ImageId(String id) {
        super(id);
    }

    static public ImageId getInstance(String id) {
        return new ImageId(id);
    }

}
