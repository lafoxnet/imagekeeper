package net.lafox.imageKeeper.ds;

import java.io.Serializable;

public class AbstractId implements Serializable {
    public final static int SIZE = 4;
//    private final static int MIN_SIZE = 2;
//    private final static int MAX_SIZE = 16;

    public final static String ID_PATTERN = "[a-zA-Z0-9]{3,12}";
//    private final static Pattern ID_PATTERN_COMPILED = Pattern.compile(ID_PATTERN);
    private final String id;
    private final int hashCode;

    protected AbstractId(String id) {
//        if (id == null) throw new ImageIdException("ImageId can't be NULL");
//        int length = id.length();
//        if (length == 0) throw new ImageIdException("ImageId can't be EMPTY");
//        if (length < MIN_SIZE) throw new ImageIdException("ImageId to SHORT");
//        if (length > MAX_SIZE) throw new ImageIdException("ImageId to LONG");
//        if (!ID_PATTERN_COMPILED.matcher(id).find()) {
//            throw new ImageIdException("ImageId BAD FORMAT. Must be: " + ID_PATTERN);
//        }
        this.id = id;
        hashCode = id.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AbstractId abstractId = (AbstractId) o;
        return id.equals(abstractId.id);
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public String toString() {
        return id;
    }

    public String getId() {
        return id;
    }
}
