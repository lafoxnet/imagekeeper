package net.lafox.imageKeeper.ds;


import static net.lafox.imageKeeper.utils.CollectionUtils.newArrayList;

import java.lang.reflect.Field;
import java.net.Inet4Address;
import java.util.List;
import java.util.Set;

import net.lafox.imageKeeper.utils.IpAddressMatcher;

public class Client {

    private final String name;
    private final boolean enabled;
    private final String token;
    private final Set<ImageFormat> imageFormats;
    private final Set<IpAddressMatcher> ipAddressMatchers;

    private Client(Builder builder) {
        name = builder.name;
        enabled = builder.enabled;
        token = builder.token;
        imageFormats = builder.imageFormats;
        ipAddressMatchers = builder.ipAddressMatchers;
    }

    public static Builder Builder() {
        return new Builder();
    }

    @Override
    public String toString() {
        return "\n  Client{\n" +
                "    name='" + name + "\',\n" +
                "    enabled=" + enabled + ",\n" +
                "    token='" + token + "\',\n" +
                "    imageFormats=" + imageFormats + ",\n" +
                "    ipAddressMatchers=" + getIpAddressMatchersAsString() + ",\n" +
                "  }\n";
    }

    public String getIpAddressMatchersAsString() {
        if (ipAddressMatchers == null) return "NULL";
        List<String> res = newArrayList();

        for (IpAddressMatcher matcher : ipAddressMatchers) {
            Inet4Address inet4Address = calcInet4Address(matcher);
            res.add(inet4Address == null ? null : inet4Address.getHostAddress() + "/" + calcMMaskBits(matcher));
        }
        return res.toString();

    }

    private Inet4Address calcInet4Address(IpAddressMatcher matcher) {
        Inet4Address requiredAddress = null;
        try {
            Field field = matcher.getClass().getDeclaredField("requiredAddress");
            field.setAccessible(true);
            requiredAddress = (Inet4Address) field.get(matcher);
        } catch (NoSuchFieldException | IllegalAccessException ignored) {

        }
        return requiredAddress;
    }

    private int calcMMaskBits(IpAddressMatcher matcher) {
        int nMaskBits = 0;

        try {
            Field field = matcher.getClass().getDeclaredField("nMaskBits");
            field.setAccessible(true);
            nMaskBits = field.getInt(matcher);
        } catch (NoSuchFieldException | IllegalAccessException ignored) {
        }
        return nMaskBits;
    }

    public static final class Builder {
        private String name;
        private boolean enabled;
        private String token;
        private Set<ImageFormat> imageFormats;
        private Set<IpAddressMatcher> ipAddressMatchers;

        Builder() {
        }

        public Builder name(String val) {
            name = val;
            return this;
        }

        public Builder enabled(boolean val) {
            enabled = val;
            return this;
        }

        public Builder token(String val) {
            token = val;
            return this;
        }

        public Builder imageFormats(Set<ImageFormat> val) {
            imageFormats = val;
            return this;
        }

        public Builder ipAddressMatchers(Set<IpAddressMatcher> val) {
            ipAddressMatchers = val;
            return this;
        }

        public Client build() {
            return new Client(this);
        }
    }

    public String getName() {
        return name;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public String getToken() {
        return token;
    }

    public Set<ImageFormat> getImageFormats() {
        return imageFormats;
    }

    public Set<IpAddressMatcher> getIpAddressMatchers() {
        return ipAddressMatchers;
    }
}
