package net.lafox.imageKeeper.exceptions;

public class ImageFormatException extends ImageKeeperException {

    public ImageFormatException(String message) {
        super(message);
    }

    public ImageFormatException(String message, Throwable cause) {
        super(message, cause);
    }

}
