package net.lafox.imageKeeper.exceptions;

public class ClintConfigPathNotExistsException extends ImageKeeperException {
    public ClintConfigPathNotExistsException(String message) {
        super(message);
    }

    public ClintConfigPathNotExistsException(String message, Throwable cause) {
        super(message, cause);
    }
}
