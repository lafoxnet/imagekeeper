package net.lafox.imageKeeper.exceptions;

public class FileUploadException extends ImageKeeperException {
    public FileUploadException(String message) {
        super(message);
    }

    public FileUploadException(String message, Throwable cause) {
        super(message, cause);
    }
}
