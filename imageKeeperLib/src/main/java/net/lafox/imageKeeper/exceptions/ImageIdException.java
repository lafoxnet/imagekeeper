package net.lafox.imageKeeper.exceptions;


public class ImageIdException extends ImageKeeperException {

    public ImageIdException(String message) {
        super(message);
    }

    public ImageIdException(String message, Throwable cause) {
        super(message, cause);
    }

}
