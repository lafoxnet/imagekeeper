package net.lafox.imageKeeper.exceptions;

public class UnavailableImageFormatException extends ImageKeeperException {

    public UnavailableImageFormatException(String message) {
        super(message);
    }

    public UnavailableImageFormatException(String message, Throwable cause) {
        super(message, cause);
    }

}
