package net.lafox.imageKeeper.exceptions;

public class BadClientException extends ImageKeeperException {
    private String description;

    public BadClientException(String message) {
        super(message);
        this.description = message;
    }

    public BadClientException(String message, String description) {
        super(message);
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public BadClientException(String message, Throwable cause) {
        super(message, cause);
    }

}
