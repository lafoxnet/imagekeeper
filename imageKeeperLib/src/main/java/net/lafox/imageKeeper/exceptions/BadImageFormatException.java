package net.lafox.imageKeeper.exceptions;


public class BadImageFormatException extends ImageKeeperException {
    public BadImageFormatException(String message) {
        super(message);
    }

    public BadImageFormatException(String message, Throwable cause) {
        super(message, cause);
    }

}
