package net.lafox.imageKeeper.exceptions;

@SuppressWarnings("WeakerAccess")
public class ImageKeeperException extends RuntimeException {
    public ImageKeeperException(String message) {
        super(message);
    }

    public ImageKeeperException(String message, Throwable cause) {
        super(message, cause);
    }

    public ImageKeeperException(Throwable cause) {
        super(cause);
    }
}
