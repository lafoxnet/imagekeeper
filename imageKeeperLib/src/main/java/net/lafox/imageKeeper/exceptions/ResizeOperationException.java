package net.lafox.imageKeeper.exceptions;

public class ResizeOperationException extends ImageKeeperException {

    public ResizeOperationException(String message) {
        super(message);
    }

    public ResizeOperationException(String message, Throwable cause) {
        super(message, cause);
    }

}
