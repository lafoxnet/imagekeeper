package net.lafox.imageKeeper.configuration;

import static net.lafox.imageKeeper.utils.CollectionUtils.newArrayList;
import static net.lafox.imageKeeper.utils.ConfigUtils.stringToBoolean;

import java.io.File;
import java.io.FileInputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReentrantLock;

import net.lafox.imageKeeper.ds.Client;
import net.lafox.imageKeeper.ds.ImageFormat;
import net.lafox.imageKeeper.exceptions.ClintConfigPathNotExistsException;
import net.lafox.imageKeeper.utils.IpAddressMatcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class ConfigService {
    private static final Logger LOGGER = LoggerFactory.getLogger(ConfigService.class);

    private static final String LIST_SPLIT_REGEX = "\\s*[,;|\\s]\\s*";

    private final ReentrantLock LOCK = new ReentrantLock();
    private final Map<String, Client> tokenClientMap = new ConcurrentHashMap<>(); //ClientToken, Client
    private final Map<String, Client> nameClientMap = new ConcurrentHashMap<>();  //ClientName,  Client

    private final File imagePathOriginal;
    private final File filePath;
    private final File imagePathConverted;
    private final Path clientConfigPath;

    public ConfigService(
            @Value("${image.path.original:/tmp/imageKeeper/Original}") File imagePathOriginal,
            @Value("${file.path:/tmp/imageKeeper/Files}") File filePath,
            @Value("${image.path.converted:/tmp/imageKeeper/Converted}") File imagePathConverted,
            @Value("${client.properties.path:classpath:clients/}") String clientPath
    ) {
        this.imagePathOriginal = imagePathOriginal;
        this.filePath = filePath;
        this.imagePathConverted = imagePathConverted;
        this.clientConfigPath = calcClintConfigPath(clientPath);
        LOGGER.info(toString());
        reloadClients();

    }

    @Override
    public String toString() {
        return "\nConfigServiceImpl{" +
                "\n\t imagePathOriginal  = " + imagePathOriginal +
                "\n\t filePath           = " + filePath +
                "\n\t imagePathConverted = " + imagePathConverted +
                "\n\t clientConfigPath   = " + clientConfigPath +
                "\n}";
    }

    private Path calcClintConfigPath(String clientPath) {

        Path path = Paths.get(clientPath);
        if (clientPath.startsWith("classpath:")) {
            URL url = ClassLoader.class.getResource(clientPath.replace("classpath:", "/"));
            try {
                path = Paths.get(url.toURI());
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
        }


        if (path == null || Files.notExists(path)) {
            String message = "Client config path '" + clientPath + "' not exists. Please specify client.properties.path variable in application.properties file";
            LOGGER.error(message);
            throw new ClintConfigPathNotExistsException(message);
        }
        return path;
    }

    void reloadClients() {
        LOGGER.info("Loading client configs from path: " + clientConfigPath);

        try {
            List<Client> res = newArrayList();
            for (File file : clientConfigPath.toFile().listFiles()) {
                Properties properties = new Properties();
                properties.load(new FileInputStream(file));

                Client client = Client.Builder()
                        .name(properties.getProperty("client.name"))
                        .enabled(stringToBoolean(properties.getProperty("client.enabled")))
                        .token(properties.getProperty("client.token"))
                        .ipAddressMatchers(calcIpAddressMatchers(properties.getProperty("client.networks")))
                        .imageFormats(calcImageFormats(properties.getProperty("client.formats")))
                        .build();
                res.add(client);
            }
            if (!res.isEmpty()) {
                LOCK.lock();
                tokenClientMap.clear();
                nameClientMap.clear();
                for (Client client : res) {
                    tokenClientMap.put(client.getToken(), client);
                    nameClientMap.put(client.getName(), client);
                }

                LOCK.unlock();
                LOGGER.info("Client configs reloaded from path: " + clientConfigPath + "\n" + nameClientMap.values().toString());
            }
        } catch (Exception e) {
            LOGGER.error("Can't find hosts properties in path:" + clientConfigPath, e);
        }
    }

    public Path getClientConfigPath() {
        return clientConfigPath;
    }

    public Set<Client> getClients() {
        return new HashSet<>(tokenClientMap.values());
    }

    public Client getClientByToken(String clientToken) {
        return tokenClientMap.get(clientToken);
    }

    public Client getClientByName(String clientToken) {
        return nameClientMap.get(clientToken);
    }

    public File getImagePathOriginal() {
        return imagePathOriginal;
    }

    public File getFilePathOriginal() {
        return filePath;
    }

    public File getImagePathConverted() {
        return imagePathConverted;
    }


    // --------------------
    private Set<ImageFormat> calcImageFormats(String formats) {
        Set<ImageFormat> res = new HashSet<>();
        for (String format : formats.split(LIST_SPLIT_REGEX)) {
            res.add(new ImageFormat(format));
        }
        return res;
    }

    private Set<IpAddressMatcher> calcIpAddressMatchers(String networksString) {
        Set<IpAddressMatcher> ipAddressMatchers = new HashSet<>();
        for (String subnet : networksString.split(LIST_SPLIT_REGEX)) {
            ipAddressMatchers.add(new IpAddressMatcher(subnet));
        }
        return ipAddressMatchers;
    }

}
