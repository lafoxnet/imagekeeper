package net.lafox.imageKeeper.configuration;

import java.nio.file.Path;
import javax.annotation.PostConstruct;

import net.lafox.imageKeeper.pathwatcher.PathWatcher;
import net.lafox.imageKeeper.pathwatcher.PathWatcherEventType;
import net.lafox.imageKeeper.pathwatcher.PathWatcherException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PathWatcherService {

    @Autowired
    private PathWatcher pathWatcher;

    @Autowired
    private ConfigService configService;

    @Autowired
    private ConfigReloader configReloader;

    @PostConstruct
    void init() {
        Path path = configService.getClientConfigPath();
        try {
            pathWatcher.subscribe(PathWatcherEventType.ENTRY_CREATE, configReloader);
            pathWatcher.subscribe(PathWatcherEventType.ENTRY_DELETE, configReloader);
            pathWatcher.subscribe(PathWatcherEventType.ENTRY_MODIFY, configReloader);
            pathWatcher.checkExisted(path);
            pathWatcher.start(path);
        } catch (Exception e) {
            throw new PathWatcherException("Cant start PathWatcher for path: '" + path + "'", e);
        }
    }
}
