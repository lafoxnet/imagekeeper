package net.lafox.imageKeeper.configuration;

import java.nio.file.Path;

import net.lafox.imageKeeper.pathwatcher.PathWatcherEventType;
import net.lafox.imageKeeper.pathwatcher.PathWatcherSubscriber;
import net.lafox.imageKeeper.pathwatcher.PathWatcherUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ConfigReloader implements PathWatcherSubscriber {
    @Autowired
    private ConfigService configService;

    @Override
    public void handler(PathWatcherEventType pathWatcherEventType, Path path) {

        if (PathWatcherUtil.isValidFileName(path, "", ".properties")) {
            configService.reloadClients();
        }


    }
}
