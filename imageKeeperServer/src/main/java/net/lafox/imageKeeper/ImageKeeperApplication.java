package net.lafox.imageKeeper;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync

public class ImageKeeperApplication {

    public static void main(String[] args) {
        SpringApplication.run(ImageKeeperApplication.class, args);
    }
}
