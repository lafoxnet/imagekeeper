package net.lafox.imageKeeper.utils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DirUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(DirUtils.class);

    public static void mkDir(File file) {
        if (Files.isDirectory(file.toPath())) return;
        forceMkDir(file);
        LOGGER.info("Creating dir: {} ", file.getAbsolutePath());
    }

    private static void forceMkDir(File file) {
        try {
            FileUtils.forceMkdir(file);
        } catch (IOException e) {
            throw new RuntimeException("Can't create Dir: " + file.getAbsolutePath(), e);
        }
    }

}
