package net.lafox.imageKeeper.utils;


import net.lafox.imageKeeper.utils.apache.RandomStringUtils;

public class IdUtils {
    public static String generateId(int size) {
        return RandomStringUtils.randomAlphanumeric(size);
    }
}
