package net.lafox.imageKeeper.utils;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import javax.imageio.ImageIO;

public class ImageUtils {

    public static Dimension imgDimension(final File file) {
        try {
            BufferedImage img = ImageIO.read(file);
            return new Dimension(img.getWidth(), img.getHeight());
        } catch (IOException e) {
            return new Dimension(0, 0);
        }
    }

    public static Dimension imgDimension(final byte[] imageInByte) {
        try {
            InputStream in = new ByteArrayInputStream(imageInByte);
            BufferedImage img = ImageIO.read(in);
            return new Dimension(img.getWidth(), img.getHeight());
        } catch (IOException ignored) {
            return new Dimension(0, 0);
        }
    }

    public static int i(double x) {
        return (int) Math.round(x);
    }

}
