package net.lafox.imageKeeper.utils;

import java.io.IOException;
import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;

import net.lafox.imageKeeper.ds.Client;
import net.lafox.imageKeeper.ds.FileDto;
import net.lafox.imageKeeper.ds.ImageDto;
import net.lafox.imageKeeper.exceptions.BadClientException;
import net.lafox.imageKeeper.exceptions.ImageFormatException;
import org.springframework.web.multipart.MultipartFile;

public class Validator {

    public static void verifyImage(MultipartFile file, Client client, HttpServletRequest request) throws IOException {
        if (ImageIO.read(file.getInputStream()) == null) {
            throw new BadClientException("Unsupported format: " + file.getName() + "Client: '" + client.getName() + "' remoteAddr: " + request.getRemoteAddr());
        }
    }

    public static void verifyClient(Client client, String token, HttpServletRequest request) {
        if (client == null) {
            throw new BadClientException("Can'f find Client by token:" + token);
        } else if (!client.isEnabled()) {
            throw new BadClientException("Client '" + client.getName() + "' is not enabled");
        } else if (!isAllowedIp(client, request.getRemoteAddr())) {
            throw new BadClientException(
                    "Client '" + client.getName() + "' not in allowed list. See logs for details",
                    "Client '" + client.getName() + "' remoteAddr: " + request.getRemoteAddr() + " not in allowed list: " + client.getIpAddressMatchersAsString()
            );
        }
    }

    public static void verifyFile(MultipartFile file, Client client, HttpServletRequest request) {
        //todo
    }

    private static boolean isAllowedIp(Client client, String ip) {
        for (IpAddressMatcher ipAddressMatcher : client.getIpAddressMatchers()) {
            if (ipAddressMatcher.matches(ip)) {
                return true;
            }
        }
        return false;
    }

    public static void validateClient(ImageDto imageDto) {
        if (imageDto.getClient() == null) {
            throw new BadClientException(imageDto + " skipped because unknown client.");
        }
    }

    public static void validateClient(FileDto fileDto) {
        if (fileDto.getClient() == null) {
            throw new BadClientException(fileDto + " skipped because unknown client.");
        }
    }

    public static void validateImageFormat(ImageDto imageDto) {
        if (!imageDto.getClient().getImageFormats().contains(imageDto.getImageFormat())) {
            throw new ImageFormatException(imageDto + " skipped because format '" + imageDto.getImageFormat() + "' is not available for client '"
                    + imageDto.getClient().getName() + "' allowed formats: " + imageDto.getClient().getImageFormats());
        }
    }
}
