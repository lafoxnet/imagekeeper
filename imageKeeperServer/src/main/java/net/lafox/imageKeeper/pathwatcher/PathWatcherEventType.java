package net.lafox.imageKeeper.pathwatcher;

public enum PathWatcherEventType {
    ENTRY_CREATE, ENTRY_DELETE, ENTRY_MODIFY
}
