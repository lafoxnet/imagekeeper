package net.lafox.imageKeeper.pathwatcher;

@SuppressWarnings("unused")
public class PathWatcherException extends RuntimeException {
    public PathWatcherException(String message) {
        super(message);
    }

    public PathWatcherException(String message, Throwable cause) {
        super(message, cause);
    }
}
