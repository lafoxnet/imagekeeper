package net.lafox.imageKeeper.pathwatcher;

import java.nio.file.Path;

public class PathWatcherUtil {

    public static boolean isValidFileName(Path path, String validFileNameStart, String validFileNameEnd) {
        String fileName = path.getFileName().toString();
        if (fileName.startsWith(".")) return false;
        return fileName.startsWith(validFileNameStart) && fileName.endsWith(validFileNameEnd);
    }
}
