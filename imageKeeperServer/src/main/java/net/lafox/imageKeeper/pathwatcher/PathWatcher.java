package net.lafox.imageKeeper.pathwatcher;

import java.nio.file.Path;

@SuppressWarnings("unused")
public interface PathWatcher {
    void subscribe(PathWatcherEventType watchEventKid, PathWatcherSubscriber subscriber);

    void unSubscribe(PathWatcherEventType watchEventKid, PathWatcherSubscriber subscriber);

    boolean isStarted();

    void checkExisted(Path uploadDir);

    void start(Path uploadDir);
}
