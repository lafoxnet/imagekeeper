package net.lafox.imageKeeper.pathwatcher;

import java.nio.file.Path;

public interface PathWatcherSubscriber {
    void handler(PathWatcherEventType pathWatcherEventType, Path path);
}
