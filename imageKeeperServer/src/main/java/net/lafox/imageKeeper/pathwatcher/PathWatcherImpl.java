package net.lafox.imageKeeper.pathwatcher;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class PathWatcherImpl implements PathWatcher {
    private final static Logger LOGGER = LoggerFactory.getLogger(PathWatcherImpl.class);
    private final Multimap<PathWatcherEventType, PathWatcherSubscriber> subscribers = ArrayListMultimap.create();
    private boolean isStarted = false;

    @Override
    public void subscribe(PathWatcherEventType watchEventKid, PathWatcherSubscriber subscriber) {
        subscribers.put(watchEventKid, subscriber);
    }

    @Override
    public void unSubscribe(PathWatcherEventType watchEventKid, PathWatcherSubscriber subscriber) {
        subscribers.remove(watchEventKid, subscriber);
    }

    @Override
    public boolean isStarted() {
        return isStarted;
    }

    @Override
    public void checkExisted(Path uploadDir) {
        try (DirectoryStream<Path> stream = Files.newDirectoryStream(uploadDir)) {
            for (Path entry : stream) {
                processEvent(entry, PathWatcherEventType.ENTRY_CREATE);
            }
        } catch (IOException e) {
            LOGGER.error("File Processing Error:", e);
        }
    }

    @SuppressWarnings("LoopConditionNotUpdatedInsideLoop")
    @Async
    @Override
    public void start(Path uploadDir) {
        if (isStarted) return;
        try (WatchService watchService = FileSystems.getDefault().newWatchService()) {
            isStarted = true;
            uploadDir.register(
                    watchService,
                    StandardWatchEventKinds.ENTRY_CREATE,
                    StandardWatchEventKinds.ENTRY_DELETE,
                    StandardWatchEventKinds.ENTRY_MODIFY
            );

            for (WatchKey key = watchService.take(); key != null; key.reset()) {
                for (WatchEvent<?> event : key.pollEvents()) {
                    processEvent(event, uploadDir);
                }
            }

        } catch (InterruptedException x) {
            LOGGER.warn("PathWatcher was Interrupted ");
        } catch (IOException e) {
            LOGGER.error("PathWatcher stopped because uploadDir '" + uploadDir + "' does not exist", e);
        } finally {
            isStarted = false;
        }
    }

    @SuppressWarnings("unchecked")
    private void processEvent(WatchEvent<?> event, Path path) {
        processEvent(path.resolve(((WatchEvent<Path>) event).context()), PathWatcherEventType.valueOf(event.kind().name()));
    }

    private void processEvent(Path filePath, PathWatcherEventType pathWatcherEventType) {
        if (subscribers.containsKey(pathWatcherEventType)) {
            for (PathWatcherSubscriber subscriber : subscribers.get(pathWatcherEventType)) {
                subscriber.handler(pathWatcherEventType, Paths.get(filePath.toString()));
            }
        }
    }
}
