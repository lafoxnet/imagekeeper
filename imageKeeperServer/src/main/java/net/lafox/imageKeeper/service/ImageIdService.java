package net.lafox.imageKeeper.service;

import static org.apache.commons.io.FileUtils.listFiles;

import java.io.File;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import net.lafox.imageKeeper.configuration.ConfigService;
import net.lafox.imageKeeper.ds.Client;
import net.lafox.imageKeeper.ds.ImageId;
import net.lafox.imageKeeper.exceptions.ImageKeeperException;
import net.lafox.imageKeeper.utils.IdUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class ImageIdService {
    private static final Logger LOGGER = LoggerFactory.getLogger(ImageIdService.class.getName());

    private static final boolean RECURSIVE = true;
    private static final String[] NO_FILTER_BY_EXT = null;
    private final Map<ImageId, String> imageIdClientNameMap = new ConcurrentHashMap<>();

    private final ConfigService configService;
    private final FileSystemImageService fileSystemImageService;

    @Autowired
    public ImageIdService(ConfigService configService, FileSystemImageService fileSystemImageService) {
        this.configService = configService;
        this.fileSystemImageService = fileSystemImageService;
        populateIdClientStorage();
    }


    private void populateIdClientStorage() {
        LOGGER.info("Scanning: total unique Images ID's: ----------------------");
        for (Client client : configService.getClients()) {
            LOGGER.info(" * client: {}", client);
            File originalDir = fileSystemImageService.getOriginalDir(client);
            LOGGER.info("    - Scanning path: {}", originalDir);
            Collection<File> files = listFiles(originalDir, NO_FILTER_BY_EXT, RECURSIVE);
            LOGGER.info("    - Files found: {}", files.size());
            for (File file : files) {
                ImageId imageId = ImageId.getInstance(file.getName());
                imageIdClientNameMap.put(imageId, client.getName());
            }
        }
        LOGGER.info("Total Found {}", imageIdClientNameMap.size());
        LOGGER.info("Scanning done ---------------------------------------------");
    }

    private ImageId generateImageId() {
        return ImageId.getInstance(IdUtils.generateId(ImageId.SIZE));
    }

    public ImageId generateUniqueImageId() {
        ImageId id;
        do {
            id = generateImageId();
        } while (imageIdClientNameMap.containsKey(id));
        return id;
    }



    public void put(ImageId imageId, Client client) {
        imageIdClientNameMap.put(imageId, client.getName());
    }


    public Client getClientById(ImageId imageId) {
        String clientToken = imageIdClientNameMap.get(imageId);
        if (clientToken == null){
            throw new ImageKeeperException("Image not exists ID: "+imageId);
        }
        return configService.getClientByName(clientToken);
    }

}
