package net.lafox.imageKeeper.service.resizer;


import net.lafox.imageKeeper.enums.ResizeOperation;
import net.lafox.imageKeeper.exceptions.ResizeOperationException;
import net.lafox.imageKeeper.service.resizer.imp.ResizerCrop;
import net.lafox.imageKeeper.service.resizer.imp.ResizerExpand;
import net.lafox.imageKeeper.service.resizer.imp.ResizerHeight;
import net.lafox.imageKeeper.service.resizer.imp.ResizerOriginal;
import net.lafox.imageKeeper.service.resizer.imp.ResizerWidth;

public class ResizerFactory {
    public static Resizer getNewResizer(ResizeOperation resizeOperation) {
        switch (resizeOperation) {
            case ORIGINAL:
                return new ResizerOriginal();
            case WIDTH:
                return new ResizerWidth();
            case HEIGHT:
                return new ResizerHeight();
            case CROP:
                return new ResizerCrop();
            case EXPAND:
                return new ResizerExpand();
            default:
                throw new ResizeOperationException("Unsupported operation:" + resizeOperation);
        }
    }
}
