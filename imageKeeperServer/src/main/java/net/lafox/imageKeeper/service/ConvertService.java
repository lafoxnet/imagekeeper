package net.lafox.imageKeeper.service;

import java.io.File;

import net.lafox.imageKeeper.ds.ImageDto;
import net.lafox.imageKeeper.service.resizer.ResizerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ConvertService {

    @Autowired
    private FileSystemImageService fileSystemImageService;

    public long convertImage(ImageDto imageDto) {
        long start = System.currentTimeMillis();
        File fileIn = fileSystemImageService.getOriginalFile(imageDto.getClient(), imageDto.getImageId());
        File fileOut = fileSystemImageService.getConvertedFile(imageDto);
        ResizerFactory
                .getNewResizer(imageDto.getImageFormat().getResizeOperation())
                .resize(fileIn, fileOut, imageDto.getImageFormat());
        return System.currentTimeMillis() - start;
    }

}
