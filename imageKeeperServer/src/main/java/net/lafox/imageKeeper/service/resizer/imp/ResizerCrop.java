package net.lafox.imageKeeper.service.resizer.imp;

import static net.lafox.imageKeeper.utils.ImageUtils.i;
import static net.lafox.imageKeeper.utils.ImageUtils.imgDimension;

import java.awt.*;
import java.io.File;

import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.geometry.Positions;
import net.lafox.imageKeeper.ds.ImageFormat;
import net.lafox.imageKeeper.service.resizer.Resizer;

public class ResizerCrop implements Resizer {
    @Override
    public void resize(File fileIn, File fileOut, ImageFormat imageFormat) {
        try {
            int width = imageFormat.getWidth();
            int height = imageFormat.getHeight();
            Dimension dim = imgDimension(fileIn);
            double wOrig = dim.getWidth();
            double hOrig = dim.getHeight();
            double wCrop, hCrop;
            double kOrig = wOrig / hOrig;
            double kQuery = (1.0 * width) / (1.0 * height);
            if (kOrig > kQuery) {
                hCrop = hOrig;
                wCrop = hCrop * kQuery;
            } else {
                wCrop = wOrig;
                hCrop = wCrop / kQuery;
            }

            Thumbnails.of(fileIn)
                    .sourceRegion(Positions.CENTER, i(wCrop), i(hCrop))
                    .forceSize(width, height)
                    .outputQuality(imageFormat.getQualityAsDouble())
                    .outputFormat(imageFormat.getExt())
                    .toFile(fileOut);

        } catch (Exception e) {
            throw new RuntimeException("Can not convert file: "
                    + fileIn.getAbsolutePath()
                    + " --> " + fileOut.getAbsolutePath()
                    + "; imageFormat:" + imageFormat,
                    e);
        }
    }
}
