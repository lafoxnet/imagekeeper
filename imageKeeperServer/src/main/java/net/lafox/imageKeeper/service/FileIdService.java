package net.lafox.imageKeeper.service;

import static org.apache.commons.io.FileUtils.listFiles;

import java.io.File;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import net.lafox.imageKeeper.configuration.ConfigService;
import net.lafox.imageKeeper.ds.Client;
import net.lafox.imageKeeper.ds.FileId;
import net.lafox.imageKeeper.ds.ImageId;
import net.lafox.imageKeeper.utils.IdUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class FileIdService {
    private static final Logger LOGGER = LoggerFactory.getLogger(FileIdService.class.getName());

    private static final boolean RECURSIVE = true;
    private static final String[] NO_FILTER_BY_EXT = null;
    private final Map<FileId, String> fileIdClientNameMap = new ConcurrentHashMap<>();

    private final ConfigService configService;
    private final FileSystemFileService fileSystemService;

    @Autowired
    public FileIdService(ConfigService configService, FileSystemFileService fileSystemService) {
        this.configService = configService;
        this.fileSystemService = fileSystemService;
        populateIdClientStorage();
    }


    private void populateIdClientStorage() {
        for (Client client : configService.getClients()) {
            for (File file : listFiles(fileSystemService.getOriginalDir(client), NO_FILTER_BY_EXT, RECURSIVE)) {
                FileId fileId = FileId.getInstance(file.getName());
                fileIdClientNameMap.put(fileId, client.getName());
            }
        }
        LOGGER.info("Found {} unique File ID's", fileIdClientNameMap.size());
    }

    private FileId generateFileId() {
        return FileId.getInstance(IdUtils.generateId(ImageId.SIZE));
    }


    public FileId generateUniqueFileId() {
        FileId id;
        do {
            id = generateFileId();
        } while (fileIdClientNameMap.containsKey(id));
        return id;
    }

    public void put(FileId fileId, Client client) {
        fileIdClientNameMap.put(fileId, client.getName());
    }


    public Client getClientById(FileId fileId) {
        return configService.getClientByName(fileIdClientNameMap.get(fileId));
    }
}
