package net.lafox.imageKeeper.service.resizer.imp;

import static net.lafox.imageKeeper.utils.ImageUtils.i;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;

import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.geometry.Positions;
import net.lafox.imageKeeper.ds.ImageFormat;
import net.lafox.imageKeeper.service.resizer.Resizer;

public class ResizerExpand implements Resizer {
    @Override
    public void resize(File fileIn, File fileOut, ImageFormat imageFormat) {
        try {
            int width = imageFormat.getWidth();
            int height = imageFormat.getHeight();

            BufferedImage src = ImageIO.read(fileIn);

            double wOrig = (double) src.getWidth();
            double hOrig = (double) src.getHeight();
            double wCrop, hCrop;
            double kOrig = wOrig / hOrig;
            double kQuery = width / (double) height;
            if (kOrig > kQuery) {
                wCrop = wOrig;
                hCrop = wCrop / kQuery;
            } else {
                hCrop = hOrig;
                wCrop = hCrop * kQuery;
            }

            BufferedImage biNew = new BufferedImage(i(wCrop), i(hCrop), BufferedImage.TYPE_INT_ARGB);
            Graphics2D g = biNew.createGraphics();
            g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC));
            if (!"png".equals(imageFormat.getExt())) {
                g.setBackground(new Color(255, 0, 255, 0));
                g.fillRect(0, 0, i(wCrop), i(hCrop));
            }
            g.drawImage(src, i((wCrop - wOrig) / 2), i((hCrop - hOrig) / 2), null);
            Thumbnails.of(biNew)
                    .sourceRegion(Positions.CENTER, i(wCrop), i(hCrop))
                    .forceSize(width, height)
                    .outputQuality(imageFormat.getQualityAsDouble())
                    .outputFormat(imageFormat.getExt())
                    .toFile(fileOut);
        } catch (Exception e) {
            throw new RuntimeException("Can not convert file: "
                    + fileIn.getAbsolutePath()
                    + " --> " + fileOut.getAbsolutePath()
                    + "; imageFormat:" + imageFormat,
                    e);
        }
    }
}
