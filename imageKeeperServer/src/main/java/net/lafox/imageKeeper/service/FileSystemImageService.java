package net.lafox.imageKeeper.service;

import java.io.File;

import net.lafox.imageKeeper.configuration.ConfigService;
import net.lafox.imageKeeper.ds.Client;
import net.lafox.imageKeeper.ds.ImageDto;
import net.lafox.imageKeeper.ds.ImageId;
import net.lafox.imageKeeper.utils.DirUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class FileSystemImageService {
    private final ConfigService configService;

    @Autowired
    public FileSystemImageService(ConfigService configService) {
        this.configService = configService;
        mkOriginalDirs();
        mkConvertedDirs();
    }


    File getOriginalDir(Client client) {
        return new File(configService.getImagePathOriginal() + File.separator + client.getName());
    }


    File getConvertedFile(ImageDto imageDto) {
        return new File(getConvertedFileName(imageDto));
    }

    public String getConvertedFileName(ImageDto imageDto) {
        return getConvertedDirName(imageDto.getClient()) + File.separator + imageDto.getImageId() + "-" + imageDto.getImageFormat();
    }

    public File getOriginalFile(Client client, ImageId imageId) {
        return new File(getOriginalFileName(client, imageId));
    }

    public String getOriginalFileName(Client client, ImageId imageId) {
        File path = getOriginalDir(client);
        return path.getAbsolutePath() + File.separator + imageId;
    }


    public boolean isImageAlreadyConverted(ImageDto imageDto) {
        return getConvertedFile(imageDto).isFile();
    }


    private void mkOriginalDirs() {
        for (Client client : configService.getClients()) {
            DirUtils.mkDir(getOriginalDir(client));
        }
    }

    private void mkConvertedDirs() {
        for (Client client : configService.getClients()) {
            DirUtils.mkDir(getConvertedDir(client));
        }
    }

    private File getConvertedDir(Client client) {
        return new File(getConvertedDirName(client));
    }


    private String getConvertedDirName(Client client) {
        return configService.getImagePathConverted() + File.separator + client.getName();
    }

}
