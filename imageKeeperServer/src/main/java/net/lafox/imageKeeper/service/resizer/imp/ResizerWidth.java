package net.lafox.imageKeeper.service.resizer.imp;

import java.io.File;

import net.coobird.thumbnailator.Thumbnails;
import net.lafox.imageKeeper.ds.ImageFormat;
import net.lafox.imageKeeper.service.resizer.Resizer;

public class ResizerWidth implements Resizer {
    @Override
    public void resize(File fileIn, File fileOut, ImageFormat imageFormat) {
        try {
            Thumbnails.of(fileIn)
                    .width(imageFormat.getWidth())
                    .outputQuality(imageFormat.getQualityAsDouble())
                    .outputFormat(imageFormat.getExt())
                    .toFile(fileOut);
        } catch (Exception e) {
            throw new RuntimeException("Can not convert file: "
                    + fileIn.getAbsolutePath()
                    + " --> " + fileOut.getAbsolutePath()
                    + "; imageFormat:" + imageFormat,
                    e);
        }
    }
}
