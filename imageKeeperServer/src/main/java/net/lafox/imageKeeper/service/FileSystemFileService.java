package net.lafox.imageKeeper.service;

import java.io.File;

import net.lafox.imageKeeper.configuration.ConfigService;
import net.lafox.imageKeeper.ds.Client;
import net.lafox.imageKeeper.ds.FileDto;
import net.lafox.imageKeeper.ds.FileId;
import net.lafox.imageKeeper.utils.DirUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FileSystemFileService {

    private final ConfigService configService;

    @Autowired
    public FileSystemFileService(ConfigService configService) {
        this.configService = configService;
        mkOriginalDirs();
    }

    public String getOriginalFileName(Client client, FileId fileId) {
        File path = getOriginalDir(client);
        return path.getAbsolutePath() + File.separator + fileId;
    }

    public String getOriginalFileName(FileDto fileDto) {
        return getOriginalFileName(fileDto.getClient(), fileDto.getFileId());
    }

    private void mkOriginalDirs() {
        for (Client client : configService.getClients()) {
            DirUtils.mkDir(getOriginalDir(client));
        }
    }

    File getOriginalDir(Client client) {
        return new File(configService.getFilePathOriginal() + File.separator + client.getName());
    }
}
