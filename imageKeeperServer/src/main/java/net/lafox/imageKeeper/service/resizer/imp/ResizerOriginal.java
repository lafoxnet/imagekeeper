package net.lafox.imageKeeper.service.resizer.imp;

import java.io.File;

import net.lafox.imageKeeper.ds.ImageFormat;
import net.lafox.imageKeeper.service.resizer.Resizer;
import org.apache.commons.io.FileUtils;

public class ResizerOriginal implements Resizer {
    @Override
    public void resize(File fileIn, File fileOut, ImageFormat imageFormat) {
        try {
            FileUtils.copyFile(fileIn, fileOut);
        } catch (Exception e) {
            throw new RuntimeException("Can not copy file: " + fileIn.getAbsolutePath() + " --> " + fileOut.getAbsolutePath(), e);
        }
    }
}
