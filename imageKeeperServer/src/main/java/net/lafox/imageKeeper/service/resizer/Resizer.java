package net.lafox.imageKeeper.service.resizer;


import java.io.File;

import net.lafox.imageKeeper.ds.ImageFormat;

public interface Resizer {
    void resize(File fileIn, File fileOut, ImageFormat imageFormat);
}
