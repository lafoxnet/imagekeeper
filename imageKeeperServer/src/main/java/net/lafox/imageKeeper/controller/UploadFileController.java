package net.lafox.imageKeeper.controller;

import static java.util.Collections.singletonMap;

import java.nio.file.Files;
import java.nio.file.Paths;
import javax.servlet.http.HttpServletRequest;

import net.lafox.imageKeeper.configuration.ConfigService;
import net.lafox.imageKeeper.ds.Client;
import net.lafox.imageKeeper.ds.FileId;
import net.lafox.imageKeeper.exceptions.BadClientException;
import net.lafox.imageKeeper.service.FileIdService;
import net.lafox.imageKeeper.service.FileSystemFileService;
import net.lafox.imageKeeper.utils.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
public class UploadFileController {

    private static final Logger LOGGER = LoggerFactory.getLogger(UploadFileController.class);

    @Autowired
    private ConfigService configService;

    @Autowired
    private FileSystemFileService fileSystemFileService;

    @Autowired
    private FileIdService fileIdService;

    @PostMapping(path = "/uploadFile", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public Object handleFormUpload(@RequestParam MultipartFile file, @RequestParam String token, HttpServletRequest request) throws Exception {
        Client client = configService.getClientByToken(token);

        String errorMesg = verify(file, client, token, request);
        if (errorMesg != null) {
            return singletonMap("errorMsg", errorMesg);
        }
        FileId fileId = fileIdService.generateUniqueFileId();
        Files.copy(file.getInputStream(), Paths.get(fileSystemFileService.getOriginalFileName(client, fileId)));
        fileIdService.put(fileId, client);
        LOGGER.info("FileId # " + fileId + " [" + file.getSize() + " bytes] was uploaded by client '" + client.getName() + "' from: " + request.getRemoteAddr());
        return singletonMap("id", fileId.toString());
    }

    private String verify(MultipartFile file, Client client, String token, HttpServletRequest request) {
        try {
            Validator.verifyClient(client, token, request);
            Validator.verifyFile(file, client, request);
        } catch (BadClientException e) {
            LOGGER.warn(e.getDescription());
            return e.getMessage();
        }
        return null;
    }

}
