package net.lafox.imageKeeper.controller;

import static net.lafox.imageKeeper.ds.AbstractId.ID_PATTERN;

import javax.servlet.http.HttpServletResponse;

import net.lafox.imageKeeper.ds.Client;
import net.lafox.imageKeeper.ds.ImageDto;
import net.lafox.imageKeeper.ds.ImageFormat;
import net.lafox.imageKeeper.ds.ImageId;
import net.lafox.imageKeeper.service.ConvertService;
import net.lafox.imageKeeper.service.FileSystemImageService;
import net.lafox.imageKeeper.service.ImageIdService;
import net.lafox.imageKeeper.utils.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController

public class DownloadImageController {
    private final static long EXPIRE = (60 * 60 * 24 * 365) * 5; // 5 years

    private static final Logger LOGGER = LoggerFactory.getLogger(DownloadImageController.class);

    @Autowired
    private ImageIdService imageIdService;

    @Autowired
    private FileSystemImageService fileSystemImageService;

    @Autowired
    private ConvertService convertService;

    @GetMapping(value = "{id:" + ID_PATTERN + "}-{size:[wheco]\\d+[x]\\d+[q]\\d+}.{ext:png|jpg|gif}", produces = MediaType.IMAGE_JPEG_VALUE)
    public FileSystemResource getImage(
            @PathVariable String id,
            @PathVariable String size,
            @PathVariable String ext,
            final HttpServletResponse response
    ) {
        ImageId imageId = ImageId.getInstance(id);
        Client clientById = imageIdService.getClientById(imageId);
        ImageDto imageDto = ImageDto.Builder()
                .imageId(imageId)
                .imageFormat(new ImageFormat(size + "." + ext))
                .client(clientById)
                .build();

        if (!validate(imageDto)) {
            return null;
        }
        convertIfNecessary(imageDto);
        response.addHeader("Cache-Control", "Public");
        response.setDateHeader("Expires", System.currentTimeMillis() + EXPIRE * 1_0000);
        response.setHeader("ETag", "Lafox-ETag:  " + imageDto.toString());
        return new FileSystemResource(fileSystemImageService.getConvertedFileName(imageDto));
    }

    private void convertIfNecessary(ImageDto imageDto) {
        if (!fileSystemImageService.isImageAlreadyConverted(imageDto)) {
            long time = convertService.convertImage(imageDto);
            LOGGER.info(imageDto + " (" + imageDto.getClient().getName() + ")" + " converted " + time + " ms");
        } else {
            LOGGER.info(imageDto + " (" + imageDto.getClient().getName() + ")");
        }
    }

    private boolean validate(ImageDto imageDto) {
        try {
            Validator.validateClient(imageDto);
            Validator.validateImageFormat(imageDto);
            return true;
        } catch (Exception e) {
            LOGGER.warn(e.getMessage());
            return false;
        }
    }
}
