package net.lafox.imageKeeper.controller;

import javax.servlet.http.HttpServletResponse;

import net.lafox.imageKeeper.ds.AbstractId;
import net.lafox.imageKeeper.ds.FileDto;
import net.lafox.imageKeeper.ds.FileId;
import net.lafox.imageKeeper.service.FileIdService;
import net.lafox.imageKeeper.service.FileSystemFileService;
import net.lafox.imageKeeper.utils.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController

public class DownloadFileController {
    private final static long EXPIRE = (60 * 60 * 24 * 365) * 5; // 5 years

    private static final Logger LOGGER = LoggerFactory.getLogger(DownloadFileController.class);

    @Autowired
    private FileIdService fileIdService;

    @Autowired
    private FileSystemFileService fileSystemFileService;

    //    @GetMapping(value = "downloadFile/{id:[a-zA-Z0-9]{" + ImageId.SIZE + "}}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    @GetMapping(value = "downloadFile/{id:" + AbstractId.ID_PATTERN + "}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public FileSystemResource getImage(@PathVariable String id, final HttpServletResponse response) {
        FileId fileId = FileId.getInstance(id);
        FileDto fileDto = new FileDto(fileId, fileIdService.getClientById(fileId));

        if (!validate(fileDto)) return null;
        response.addHeader("Cache-Control", "Public");
        response.setDateHeader("Expires", System.currentTimeMillis() + EXPIRE * 1_0000);
        response.setHeader("ETag", "Lafox-ETag:  " + fileDto);
        return new FileSystemResource(fileSystemFileService.getOriginalFileName(fileDto));
    }


    private boolean validate(FileDto fileDto) {
        try {
            Validator.validateClient(fileDto);
            return true;
        } catch (Exception e) {
            LOGGER.warn(e.getMessage());
            return false;
        }
    }


}
