package net.lafox.imageKeeper.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import javax.imageio.ImageIO;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Sets;
import net.lafox.imageKeeper.ds.Client;
import net.lafox.imageKeeper.ds.FileId;
import net.lafox.imageKeeper.ds.ImageFormat;
import net.lafox.imageKeeper.ds.UploadResponse;
import net.lafox.imageKeeper.service.FileSystemFileService;
import net.lafox.imageKeeper.utils.IpAddressMatcher;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class DownloadFileControllerTest {
    private static final String fileName = "testTriangle.png";

    @Autowired
    private MockMvc mvc;

    @Autowired
    private FileSystemFileService fileSystemService;

    private FileId fileId;
    private File originalFile;

    @Before
    public void init() throws Exception {
        MockMultipartFile mockMultipartFile = new MockMultipartFile("file", fileName, MediaType.IMAGE_PNG_VALUE, getImageAsStream());
        MvcResult result = mvc.perform(
                multipart("/uploadFile")
                        .file(mockMultipartFile)
                        .contentType(MediaType.MULTIPART_FORM_DATA)
                        .accept(MediaType.APPLICATION_JSON_UTF8)
                        .param("token", "scienceToken")
        )
//                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.id").exists())
                .andReturn();
        String JsonString = result.getResponse().getContentAsString();

        ObjectMapper mapper = new ObjectMapper();
        UploadResponse imageJson = mapper.readValue(JsonString, UploadResponse.class);
        fileId = FileId.getInstance(imageJson.getId());
//        client = new Client("science    : enabled  : scienceToken : c640x480q50.jpg, c1920x1080q50.jpg");
        Client client = Client.Builder()
                .name("science")
                .enabled(true)
                .token("scienceToken")
                .imageFormats(Sets.newHashSet(new ImageFormat("c640x480q50.jpg"), new ImageFormat("c1920x1080q50.jpg")))
                .ipAddressMatchers(Sets.newHashSet(new IpAddressMatcher("0.0.0.0/0")))
                .build();
    }

    private InputStream getImageAsStream() {
        return ClassLoader.class.getResourceAsStream("/img/" + fileName);
    }

    @Test
    public void handleFormUpload() throws Exception {
        MvcResult result = mvc.perform(get("/downloadFile/" + fileId))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_OCTET_STREAM_VALUE))
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        ByteArrayInputStream inputStream = new ByteArrayInputStream(bytes);
        BufferedImage image = ImageIO.read(inputStream);
        Assert.assertEquals(15, image.getWidth());
        Assert.assertEquals(15, image.getHeight());
    }

    @Test
    public void handleFormUploadError1() throws Exception {
        MvcResult result = mvc.perform(get("/downloadFile/" + fileId))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_OCTET_STREAM_VALUE))
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        ByteArrayInputStream inputStream = new ByteArrayInputStream(bytes);
        BufferedImage image = ImageIO.read(inputStream);
        Assert.assertEquals(15, image.getWidth());
        Assert.assertEquals(15, image.getHeight());
    }


}