package net.lafox.imageKeeper.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import javax.imageio.ImageIO;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Sets;
import net.lafox.imageKeeper.ds.Client;
import net.lafox.imageKeeper.ds.ImageFormat;
import net.lafox.imageKeeper.ds.ImageId;
import net.lafox.imageKeeper.ds.UploadResponse;
import net.lafox.imageKeeper.service.FileSystemImageService;
import net.lafox.imageKeeper.utils.ImageUtils;
import net.lafox.imageKeeper.utils.IpAddressMatcher;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class DownloadImageControllerTest {
    private static final String fileName = "testTriangle.png";

    @Autowired
    private MockMvc mvc;

    @Autowired
    private FileSystemImageService fileSystemImageService;

    private ImageId imageId;
    private Client client;
    private File originalFile;
    private Dimension originalDimension;


    @Before
    public void init() throws Exception {
        MockMultipartFile mockMultipartFile = new MockMultipartFile("file", fileName, MediaType.IMAGE_PNG_VALUE, getImageAsStream());
        MvcResult result = mvc.perform(
                multipart("/uploadImage")
                        .file(mockMultipartFile)
                        .contentType(MediaType.MULTIPART_FORM_DATA)
                        .accept(MediaType.APPLICATION_JSON_UTF8)
                        .param("token", "scienceToken")
        )
//                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.id").exists())
                .andReturn();
        String JsonString = result.getResponse().getContentAsString();

        ObjectMapper mapper = new ObjectMapper();
        UploadResponse imageJson = mapper.readValue(JsonString, UploadResponse.class);
        imageId = ImageId.getInstance(imageJson.getId());
//        client = new Client("science    : enabled  : scienceToken : c640x480q50.jpg, c1920x1080q50.jpg");
        client = Client.Builder()
                .name("science")
                .enabled(true)
                .token("scienceToken")
                .imageFormats(Sets.newHashSet(new ImageFormat("c640x480q50.jpg"), new ImageFormat("c1920x1080q50.jpg")))
                .ipAddressMatchers(Sets.newHashSet(new IpAddressMatcher("0.0.0.0/0")))
                .build();
        originalFile = fileSystemImageService.getOriginalFile(client, imageId);

        originalDimension = ImageUtils.imgDimension(originalFile);
    }

    private InputStream getImageAsStream() {
        return ClassLoader.class.getResourceAsStream("/img/" + fileName);
    }

    @Test
    public void handleFormUpload() throws Exception {
        MvcResult result = mvc.perform(get("/" + imageId + "-c1920x1080q50.jpg"))
//                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.IMAGE_JPEG_VALUE))
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        ByteArrayInputStream inputStream = new ByteArrayInputStream(bytes);
        BufferedImage image = ImageIO.read(inputStream);
        Assert.assertEquals(1920, image.getWidth());
        Assert.assertEquals(1080, image.getHeight());
    }

    @Test
    public void handleFormUploadError1() throws Exception {
        MvcResult result = mvc.perform(get("/" + imageId + "-c111x111q50.jpg"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.IMAGE_JPEG_VALUE))
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        ByteArrayInputStream inputStream = new ByteArrayInputStream(bytes);
        BufferedImage image = ImageIO.read(inputStream);
        Assert.assertEquals(111, image.getWidth());
        Assert.assertEquals(111, image.getHeight());
    }


}