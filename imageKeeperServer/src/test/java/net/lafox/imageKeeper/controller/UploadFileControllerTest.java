package net.lafox.imageKeeper.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.InputStream;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class UploadFileControllerTest {
    private static final String fileName = "testTriangle.png";
    @Autowired
    private MockMvc mvc;

    @Test
    public void handleFormUpload() throws Exception {
        MockMultipartFile mockMultipartFile = new MockMultipartFile("file", fileName, MediaType.IMAGE_PNG_VALUE, getImageAsStream());
        mvc.perform(
                multipart("/uploadFile")
                        .file(mockMultipartFile)
                        .contentType(MediaType.MULTIPART_FORM_DATA)
                        .accept(MediaType.APPLICATION_JSON_UTF8)
                        .param("token", "scienceToken")
        )
//                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.id").exists())
        ;
    }

    private InputStream getImageAsStream() {
        return ClassLoader.class.getResourceAsStream("/img/" + fileName);
    }

}