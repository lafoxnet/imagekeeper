package net.lafox.imageKeeper;

import net.lafox.imageKeeper.utils.IdUtils;
import org.junit.Assert;
import org.junit.Test;

public class IdUtilsTest {
    @Test
    public void generateId() throws Exception {
        int size = 16;
        String result = IdUtils.generateId(size);
        Assert.assertNotNull(result);
        Assert.assertEquals(size, result.length());
        Assert.assertTrue(result.matches("\\w+"));// res contain only letters and numbers
    }

}